# DAW-7

https://webapp-php.ew.r.appspot.com

## Install

### php

```sh
sudo apt install php-cli php-curl php-gd php-mbstring
```

### composer

```sh
cd daw-7-webapp
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
``` 

## Develop

Run local:
```sh
./composer.phar update
./composer.phar start
```

Test:
```sh
curl -v  http://localhost:3000/invoice/david --output invoice.pdf
```


## Google App Engine

- https://cloud.google.com/appengine/docs/standard/php7

```sh
curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-367.0.0-linux-x86_64.tar.gz
tar xf google-cloud-sdk-367.0.0-linux-x86_64.tar.gz
mv google-cloud-sdk ~
rm google-cloud-sdk-367.0.0-linux-x86_64.tar.gz
~/google-cloud-sdk/install.sh
```

```sh
gcloud init
gcloud app create   (europe-west)
```
